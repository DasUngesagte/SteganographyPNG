#include <exception>
#include <iostream>
#include "png.hpp"


enum Mode {
    ENCODING,
    DECODING
};


void show_helppage(char *argv[]) {
    std::cout << "SteganographyPNG by DasUngesagte, Licensed under GPL3" <<
              std::endl;
    std::cout << "Usage for encoding a message:" << std::endl;
    std::cout << argv[0] << " -e [INPUT IMAGE] [INPUT TEXT] [OPTIONAL OUTPUT "
                            "IMAGE]" << std::endl;
    std::cout << "Usage for decoding a message:" << std::endl;
    std::cout << argv[0] << " -d [INPUT IMAGE] [OPTIONAL OUTPUT TEXT]" <<
              std::endl;
}


int main(int argc, char *argv[]) {

    std::string input_image;
    std::string output_image;
    std::string input_text;
    std::string output_text;
    Mode mode = ENCODING;

    if (argc < 3) {
        std::cerr << "Too few arguments" << std::endl;
        show_helppage(argv);
        return 0;
    }

    // Get the arguments:
    for (size_t i = 1; i < argc; ++i) {
        if (i + 1 >= argc) {
            std::cerr << "Too few arguments" << std::endl;
            show_helppage(argv);
            return 0;
        } else if (std::string(argv[i]) == "-e" && i + 2 < argc) {
            mode = ENCODING;
            input_image = std::string(argv[++i]);
            input_text = std::string(argv[++i]);
            if (i < argc - 1) {
                output_image = std::string(argv[++i]);
            }
            break;
        } else if (std::string(argv[i]) == "-d" && i + 1 < argc) {
            mode = DECODING;
            input_image = std::string(argv[++i]);
            if (i < argc - 1) {
                output_text = std::string(argv[++i]);
            }
            break;
        }
    }

    PNG image(input_image);

    // Give feedback:
    if (mode == ENCODING) {

        std::cout << "Encoding " << input_image << " with " << input_text;
        if (!output_image.empty()) {
            std::cout << " to " << output_image;
        }
        std::cout << "..." << std::endl;

        try {
            image.encode_message(input_text);
            std::cout << "Saving..." << std::endl;
            image.save_image(output_image);
        }
        catch (std::exception &ex) {
            std::cerr << ex.what() << std::endl;
            return 1;
        }

    } else {

        std::cout << "Decoding " << input_image;
        if (!output_text.empty()) {
            std::cout << " to " << output_text;
        }
        std::cout << "..." << std::endl;

        try {
            image.decode_message(output_text);
        }
        catch (std::exception &ex) {
            std::cerr << ex.what() << std::endl;
            return 1;
        }
    }

    return 0;
}