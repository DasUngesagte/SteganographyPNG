#include "png.hpp"


PNG::PNG(const std::string &input) : _rawdata(), _width(), _height(),
                                     _old_input(input) {

    unsigned error = lodepng::decode(_rawdata, _width, _height, input);
    if (error) throw std::invalid_argument(lodepng_error_text(error));
    // Didn't want to write an exception class...
}


void PNG::encode_data(uint8_t data, size_t x, size_t y) {

    // Get old values:
    uint8_t r1 = _rawdata[4 * _width * y + 4 * x + 0];
    uint8_t b1 = _rawdata[4 * _width * y + 4 * x + 2];
    uint8_t r2 = _rawdata[4 * _width * y + 4 * x + 4];
    uint8_t b2 = _rawdata[4 * _width * y + 4 * x + 6];

    // Extract values:
    uint8_t first_pair = (data & FIRST_TWO) >> 0;   // NOLINT
    uint8_t second_pair = (data & SECOND_TWO) >> 2; // NOLINT
    uint8_t third_pair = (data & THIRD_TWO) >> 4;   // NOLINT
    uint8_t fourth_pair = (data & FOURTH_TWO) >> 6; // NOLINT

    // Insert values:
    r1 = (r1 & DELETE_TWO) | first_pair;  // NOLINT
    b1 = (b1 & DELETE_TWO) | second_pair; // NOLINT
    r2 = (r2 & DELETE_TWO) | third_pair;  // NOLINT
    b2 = (b2 & DELETE_TWO) | fourth_pair; // NOLINT

    // Update rawdata:
    _rawdata[4 * _width * y + 4 * x + 0] = r1;
    _rawdata[4 * _width * y + 4 * x + 2] = b1;
    _rawdata[4 * _width * y + 4 * x + 4] = r2;
    _rawdata[4 * _width * y + 4 * x + 6] = b2;
}


uint8_t PNG::decode_data(size_t x, size_t y) {

    uint8_t result = 0;

    // Get values:
    uint8_t r1 = _rawdata[4 * _width * y + 4 * x + 0];
    uint8_t b1 = _rawdata[4 * _width * y + 4 * x + 2];
    uint8_t r2 = _rawdata[4 * _width * y + 4 * x + 4];
    uint8_t b2 = _rawdata[4 * _width * y + 4 * x + 6];

    // Extract bits:
    r1 = (r1 & FIRST_TWO) << 0;  // NOLINT
    b1 = (b1 & FIRST_TWO) << 2;  // NOLINT
    r2 = (r2 & FIRST_TWO) << 4;  // NOLINT
    b2 = (b2 & FIRST_TWO) << 6;  // NOLINT

    // Put it all together:
    result = r1 | b1 | r2 | b2;  // NOLINT

    return result;
}


void PNG::encode_message(std::string &input) {

    // Get the message:
    auto message_buffered = buffer_file(input);
    uint64_t message_size = message_buffered.size();

    // Simple error handling:
    if (message_size * 2 > _width * _height) {
        throw std::invalid_argument("This image is too small.");
    }

    // Encode the message:
    bool finished = false;
    for (size_t i = 0; i < _height * _width && !finished; ++ ++i) {

        // We use 2 pixel for every char:
        size_t current_counter = i / 2;

        print_progress(message_size, current_counter);

        if (current_counter < sizeof(uint64_t)) {

            // Encode the size of our message:
            auto shift = current_counter * sizeof(uint64_t);
            auto mask = static_cast<uint64_t>(0b11111111) << shift;
            auto data = static_cast<u_int8_t>((message_size & mask) >> shift);
            encode_data(data, i % _width, i / _width);

        } else {

            size_t current_char_pos = current_counter - sizeof
                    (uint64_t);  // 8 byte are already used

            // Check for EOF:
            if (current_char_pos < message_size) {
                char current_char = message_buffered[current_char_pos];
                encode_data(static_cast<uint8_t>(current_char),
                            i % _width, i / _width);
            } else {
                finished = true;
            }
        }
    }

    std::cout << std::endl;
    std::clog << message_size << " chars encoded in " << message_size * 2
              << " pixels." << std::endl;
}


void PNG::decode_message(std::string &output) {

    std::string message_buffered;
    uint64_t message_size = 0;

    // Get the message size:
    for (size_t i = 0; i < sizeof(uint64_t); ++ ++i) {

        //Extract current byte from long:
        uint64_t data = decode_data(i % _width, i / _width);
        data = data << i * sizeof(uint64_t) / 2;
        message_size = message_size | data;

    }

    // Crude error handling:
    if (message_size * 2 > _width * _height) {
        throw std::invalid_argument("Not properly encoded.");
    }

    std::clog << "There are " << message_size << " chars to decode."
              << std::endl;

    message_buffered.reserve(message_size);
    message_size += sizeof(uint64_t); // Add the encoded size

    // Decode the message:
    bool finished = false;
    for (size_t i = 0; i < _height * _width && !finished; ++ ++i) {

        char current_char = decode_data(i % _width, i / _width);

        print_progress(message_size * 2, i);

        if (i >= sizeof(uint64_t) * 2) {
            if (i < message_size * 2) {
                message_buffered += current_char;
            } else {
                finished = true;
            }
        }
    }

    save_file(output, message_buffered);
    std::cout << "\n" << std::endl;
}


void PNG::save_image(std::string &output) const {

    std::string output_path = output;
    if (output.empty()) {
        output_path = _old_input;
        size_t pos = output_path.find_last_of('.');
        output_path = output_path.replace(pos, 1, "_").append(
                "_encoded.png");
    }

    //Encode the image
    unsigned error = lodepng::encode(output_path, _rawdata, _width,
                                     _height);

    //if there's an error, display it
    if (error)
        throw std::invalid_argument("Cannot write to: " + output_path);
}


std::string PNG::buffer_file(const std::string &input) const {
    std::ifstream in(input, std::ios::in | std::ios::binary);
    if (in) {
        std::string contents;
        in.seekg(0, std::ios::end);
        contents.resize(in.tellg());
        in.seekg(0, std::ios::beg);
        in.read(&contents[0], contents.size());
        in.close();
        return (contents);
    }
    throw std::invalid_argument("Cannot open " + input);
}

void PNG::save_file(std::string &output, const std::string &buffered_file)
const {
    std::string output_path = output;
    if (output.empty()) {
        output_path = _old_input;
        size_t pos = output_path.find_last_of('.');
        output_path = output_path.replace(pos, 1, "_").append(
                "_decoded.txt");
    }

    std::ofstream message(output_path);
    if (message.is_open()) {
        message << buffered_file;
        message.close();
    } else {
        throw std::invalid_argument("Cannot write to " + output_path);
    }
}


void PNG::print_progress(size_t total, size_t current) const {
    double percentage = static_cast<double>(current) / static_cast<double>
    (total) * 100;
    long percentage_round = std::lround(percentage);
    std::cout << "\r" << percentage_round << "% Completed" << std::flush;
}