#ifndef STEGANOGRAPHYPNG_PNG_HPP
#define STEGANOGRAPHYPNG_PNG_HPP

#include <cerrno>
#include <cmath>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>
#include <sys/stat.h>
#include "lodepng.h"

class PNG {

private:
    std::vector<unsigned char> _rawdata; // RGBA
    unsigned int _height;
    unsigned int _width;

    std::string _old_input;

    void encode_data(uint8_t data, size_t x, size_t y);

    uint8_t decode_data(size_t x, size_t y);

    void print_progress(size_t total, size_t current) const;

    std::string buffer_file(const std::string &input) const;

    void save_file(std::string &output, const std::string &buffered_file) const;

    // constants:
    static const uint8_t FIRST_TWO = 0b00000011;
    static const uint8_t SECOND_TWO = 0b00001100;
    static const uint8_t THIRD_TWO = 0b00110000;
    static const uint8_t FOURTH_TWO = 0b11000000;
    static const uint8_t DELETE_TWO = 0b11111100;

public:
    PNG() = delete;

    explicit PNG(const std::string &input);

    void save_image(std::string &output) const;

    void encode_message(std::string &input);

    void decode_message(std::string &output);

};


#endif //STEGANOGRAPHYPNG_PNG_HPP
